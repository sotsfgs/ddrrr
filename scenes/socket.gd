extends Area2D

export(String, "first", "second", "third", "fourth") var action = "first"
var player_won = false

func _ready():
    $Out.connect("body_entered", self, "on_body_entered")

func _process(delta):
    if Input.is_action_pressed(action) and not player_won:
        if not $AnimationPlayer.current_animation == "pressed":
            $AnimationPlayer.play("pressed")
        for body in get_overlapping_bodies():
            if not $Sfx/Matched.playing:
                $Sfx/Matched.play()
            body.emit_signal("matched")
            body.queue_free()
    else:
        if not $AnimationPlayer.current_animation == "normal":
            $AnimationPlayer.play("normal")

func on_player_win():
    player_won = true

func on_body_entered(body):
    if not $Sfx/Mismatched.playing:
        $Sfx/Mismatched.play()
    body.emit_signal("mismatched")
    body.queue_free()