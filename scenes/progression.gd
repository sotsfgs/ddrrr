extends Area2D

func _ready():
    connect("body_entered", $"..", "update_song_progress")
    connect("body_entered", $"../CanvasLayer/UI", "on_failure")