extends Node2D

func _ready():
	Transition.play_main_music("res://assets/chinese stock car dealer.ogg", true)
	# play the music file too
	$AnimationPlayer.play("entrace")

# Play the bounce animation
func on_entrance_finish():
	$AnimationPlayer.play("bounce")

func _process(delta):
	if $AnimationPlayer.current_animation == "bounce":
		if Input.is_action_just_pressed("start"):
			Transition.fade_to("res://scenes/About controls.tscn")
		elif Input.is_action_just_pressed("exit"):
			get_tree().quit()
