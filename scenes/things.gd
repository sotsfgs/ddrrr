extends Node2D

export var holes = false
var pos = []
var hooks = ["A", "B", "C", "D"]
var difficulty = 4
var counter = 0
var vertical_space = 1
export var generalspeed = 200
onready var hole_scene = load("res://scenes/Hole.tscn")
var bpm = 60 setget set_bpm, get_bpm

func _ready():
    $Timer.connect("timeout", self, "throw")
    $Timer.wait_time = 3
    $Timer.start()

func set_bpm(value):
    bpm = value

# Return the time between beats
func get_bpm():
    var value = (60000.0 / float(bpm)) / 1000.0
    return value

# Prepare things to throw at the player, if `holes` is `false`
# then throw musical notes.
func throw():
    rand_seed(OS.get_unix_time())
    var index = randi() % hooks.size()
    var hook = hooks[index]
    if holes:
        var hole_instance = hole_scene.instance()
        hole_instance.set_name(hook)
        get_node(hook).add_child(hole_instance)
        # Use the index of the hook in the list to set the distance between objects
        var start_pos = -vertical_space * index
        print("Starting position for ", hook, " is ", start_pos)
        hole_instance.move_local_y(start_pos)
        hole_instance.SPEED = generalspeed
        counter += 1
    else:
        pass

    if counter >= difficulty:
        $Timer.wait_time = rand_range(get_bpm()*2, get_bpm()*4)
        $Timer.start()
    else:
        $Timer.wait_time = rand_range(0, get_bpm())
        $Timer.start()