extends Area2D


signal explosion
var SPEED = 0
var DIRECTION = Vector2(0, 1)

func _ready():
    $Visible.connect("screen_exited", self, "on_screen_exit")
    connect("body_entered", self, "on_car_touched")
    $AnimationPlayer.play("idle")

func on_car_touched(object):
    if object.name == "Car":
        emit_signal("explosion")
        $AnimationPlayer.play("explosion")

func _process(delta):
    var motion = SPEED * DIRECTION * delta
    move_local_y(motion.y)

func on_screen_exit():
    SPEED = 0
    queue_free()