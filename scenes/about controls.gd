extends Node2D

func _ready():
    pass

func _process(delta):
    if $AnimationPlayer.current_animation == "bounce":
        if Input.is_action_just_pressed("start"):
            Transition.fade_to("res://scenes/World.tscn")
        elif Input.is_action_just_pressed("exit"):
            Transition.fade_to("res://scenes/Main menu.tscn")