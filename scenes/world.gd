extends Node2D

signal player_win
signal player_lose

func _ready():
    Transition.stop_main_music()
    stage_setup()
    $CanvasLayer/UI.connect("nogas", self, "on_nogas")
    connect("player_win", $Car, "on_player_win")
    connect("player_lose", $Car, "on_player_lose")
    connect("player_win", self, "on_player_win")
    connect("player_lose", $"Stage 1", "on_player_lose")
    connect("player_win", $"Stage 1", "on_player_win")
    connect("player_lose", self, "on_player_lose")
    # Connect each socket to the signal when player wins
    connect("player_win", $"First socket", "on_player_win")
    connect("player_win", $"Second socket", "on_player_win")
    connect("player_win", $"Third socket", "on_player_win")
    connect("player_win", $"Fourth socket", "on_player_win")

    $Car.connect("car_screen_exited", self, "on_car_screen_exited")
    $"Stage 1/Finish".connect("body_entered", self, "on_finish_body_enter")
    # Connect signals for all movable objects in the game
    var notes = get_tree().get_nodes_in_group("notes")
    for note in notes:
        note.connect("matched", $CanvasLayer/UI, "on_success")
        note.connect("mismatched", $CanvasLayer/UI, "on_failure")
    var holes = get_tree().get_nodes_in_group("holes")
    for hole in holes:
        hole.connect("explosion", $CanvasLayer/UI, "timer_add", [10])
        hole.connect("explosion", self, "penalization")
    $AnimationPlayer.play("Line flag")

func penalization():
    $AnimationPlayer.play("penalization")

func goto_mainmenu():
    Transition.fade_to("res://scenes/Main menu.tscn")

func time_record():
    $"Player time".text = $CanvasLayer/UI/mc/vbox/hbox/Time.text + " seconds"

func restart_game():
    Transition.fade_to("res://scenes/World.tscn")

func stage_setup():
    var size = OS.window_size
    # position the walls and the player
    var walls_right_side = (size.x / 2) - 10
    var center_right_side = walls_right_side/2
    $Wall.position.x = 10
    $"Wall 2".position.x = walls_right_side
    $Car.position.x = center_right_side
    # Position the walls and the player vertically
    var vertical_center = (size.y / 2) + (size.y / 4)
    $Wall.position.y = vertical_center
    $"Wall 2".position.y = vertical_center
    $Car.position.y = vertical_center

func start_stage():
    $"Stage 1".start = true
    $CanvasLayer/UI.visible = true
    $CanvasLayer/UI.regular_drop_start()
    $Car/AnimationPlayer.play("Running")
    Transition.play_main_music("res://assets/Monplaisir_-_04_-_Level_1.ogg", false)

# When the car has exited the screen
func on_car_screen_exited():
    $AnimationPlayer.play("Player win")

func on_player_win():
    $CanvasLayer/UI.visible = false
    $CanvasLayer/UI.regular_drop_stop()
    Transition.play_main_music("res://assets/Monplaisir_-_08_-_Victory.ogg", false)

func on_player_lose():
    $CanvasLayer/UI.visible = false
    $CanvasLayer/UI.regular_drop_stop()
    Transition.play_main_music("res://assets/Monplaisir_-_09_-_Defeat.ogg", false)
    $AnimationPlayer.play("Player lose")

func on_finish_body_enter(body):
    if body.name == "Car" and int($CanvasLayer/UI/mc/vbox/gas/ProgressBar.text) > 0:
        # Player made it into the finish line
        emit_signal("player_win")

func on_nogas():
    # The player has no gas, the player lose!
    emit_signal("player_lose")