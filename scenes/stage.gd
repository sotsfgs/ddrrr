extends Node2D

export var SPEED = 200
var DIRECTION = Vector2(0, 1)
var start = false

func _process(delta):
    if start:
        var motion = SPEED * DIRECTION * delta
        move_local_y(motion.y)

func on_player_win():
    on_player_lose()

func on_player_lose():
    start = false
    for note in get_tree().get_nodes_in_group("notes"):
        note.visible = false