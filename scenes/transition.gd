extends CanvasLayer

var path = ""

func fade_to(scn_path):
    path = scn_path # store the scene path
    get_node("AnimationPlayer").play("fade out-in-out")

func change_scene():
    if path != "":
        get_tree().change_scene(path)

func stop_main_music():
    $"Music main".stop()

func play_second_music(path):
    $"Music main/Music second".stop()
    $"Music main/Music second".stream = path
    $"Music main/Music second".play()

func fade_second_music():
    $"Music main/Music second".volume_db = -80

func fade_in_second_music():
    $"Music main/Music second".volume_db = 0

func play_main_music(path, loop=false):
    var audio = load(path)
    audio.loop = loop
    $"Music main".stop()
    $"Music main".stream = audio
    $"Music main".play()