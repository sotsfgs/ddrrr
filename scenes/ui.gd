extends Container

signal nogas

func _ready():
    $Timer.connect("timeout", self, "regular_drop")
    $Timer.connect("timeout", self, "timer_add")

func timer_add(penalty=0):
    if penalty:
        $mc/vbox/hbox/Time.text = str(int($mc/vbox/hbox/Time.text) + penalty)
    else:
        $mc/vbox/hbox/Time.text = str(int($mc/vbox/hbox/Time.text) + 1)

func on_failure(value=null):
    $mc/vbox/gas/ProgressBar.text = str(int($mc/vbox/gas/ProgressBar.text)-2)
    checkgas()

func on_success():
    $mc/vbox/gas/ProgressBar.text = str(int($mc/vbox/gas/ProgressBar.text)+2)

func regular_drop():
    if int($mc/vbox/gas/ProgressBar.text) > 0:
        $mc/vbox/gas/ProgressBar.text = str(int($mc/vbox/gas/ProgressBar.text)-1)
    checkgas()

func regular_drop_start():
    $Timer.start()

func regular_drop_stop():
    $Timer.stop()

func checkgas():
    if int($mc/vbox/gas/ProgressBar.text) <= 0:
        emit_signal("nogas")