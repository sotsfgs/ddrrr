extends KinematicBody2D

signal car_screen_exited

const SIDE_SPEED = 800
var SPEED = 600
var stop_control = false

func _ready():
    $VisibilityNotifier2D.connect("screen_exited", self, "on_screen_exited")
    $Tween.connect("tween_started", self, "on_tween_started")

func _physics_process(delta):
    var motion = 0
    if not stop_control:
        var input_direction = Vector2()

        input_direction.x = float(Input.is_action_pressed("right")) - float(Input.is_action_pressed("left"))

        motion = input_direction.normalized() * SIDE_SPEED * delta
    else:
        # Take the car out of the screen
        motion = Vector2(0, -1) * SPEED * delta

    move_and_collide(motion)

func get_height():
    return $Sprite.texture.get_height() * 2

func on_player_win():
    stop_control = true

func on_player_lose():
    stop_control = true
    SPEED = 0
    $Tween.interpolate_property($Sprite, "position", Vector2(0, 0), Vector2(0, -get_height()), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
    $Tween.start()

func on_screen_exited():
    emit_signal("car_screen_exited")
    SPEED = 0
    queue_free()

func on_tween_started(object, nodepath):
    $AnimationPlayer.play("Out of gas")